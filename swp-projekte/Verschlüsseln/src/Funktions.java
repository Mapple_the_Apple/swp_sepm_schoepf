import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Funktions {
	private String gse;
	public String encrypt(String code, String pw) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		String keyStr = pw;
		byte[] key = keyStr.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key=sha.digest(key);
		key = Arrays.copyOf(key, 16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		
		String text = code;
		 
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(text.getBytes());
		
		Encoder myEncoder=Base64.getEncoder();
		byte[] geheim = myEncoder.encode(encrypted);
		
		gse=new String(geheim);
		return(gse);
		
	}
	
	public String decrypt(String pw) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Decoder myDecoder2 = Base64.getDecoder();
		byte[] crypted2 = myDecoder2.decode(gse);
		
		String keyStr = pw;
		byte[] key = keyStr.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key=sha.digest(key);
		key = Arrays.copyOf(key, 16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		
		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE,secretKeySpec);
		byte[] cipherData2 = cipher2.doFinal(crypted2);
		String erg = new String(cipherData2);
		
		return(erg);
	}
	
	public String decrypt(String message, String pw) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Decoder myDecoder2 = Base64.getDecoder();
		byte[] crypted2 = myDecoder2.decode(message);
		
		String keyStr = pw;
		byte[] key = keyStr.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key=sha.digest(key);
		key = Arrays.copyOf(key, 16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		
		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE,secretKeySpec);
		byte[] cipherData2 = cipher2.doFinal(crypted2);
		String erg = new String(cipherData2);
		
		return(erg);
	}

}
