import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Main {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		/*String keyStr = "OK";
		byte[] key = keyStr.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key=sha.digest(key);
		key = Arrays.copyOf(key, 16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		
		String text = "Ein HTL'ller der nicht trinkt, ist wie ein Sinus der nicht schwingt";
		 
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(text.getBytes());
		
		Encoder myEncoder=Base64.getEncoder();
		byte[] geheim = myEncoder.encode(encrypted);
		
		System.out.println(new String(geheim));
		
		
		Decoder myDecoder2 = Base64.getDecoder();
		byte[] crypted2 = myDecoder2.decode(geheim);
		
		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE,secretKeySpec);
		byte[] cipherData2 = cipher2.doFinal(crypted2);
		String erg = new String(cipherData2);
		
		System.out.println(erg);*/
		
		Funktions crypt = new Funktions();
		System.out.println(crypt.encrypt("Daniel stinkt", "jojojo"));
		System.out.println(crypt.decrypt("jojojo"));
		System.out.println(crypt.decrypt("9ZXsMcYDxwQk2mrMW/vKpK5txEwW31pzQlaqWuHG220=","ich"));
		
	}

}
