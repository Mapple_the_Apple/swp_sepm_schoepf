import java.util.Scanner;

public class main {

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		FahrzeugLotView vlv= new FahrzeugLotView();
		vlv.add_new_Vehicle(new PKW("BMW", 2014, "M3", "IL 34LIKE","Radio"));
		vlv.add_new_Vehicle(new PKW("VW", 2009, "VW Passat", "IL 9XYZ","Bluetooth, GPS"));
		vlv.add_new_Vehicle(new Motorrad("Kawasaki", 2016, "Ninja H2R","I L0VE",993));
		vlv.add_new_Vehicle(new Motorrad("KTM", 2014, "EXC","OO1",590));
		vlv.add_new_Vehicle(new Transporter("Mercedes", 2005, "Mercedes Transporter","I OLO54",3, 12));
		
		System.out.println("Commandos:");
		System.out.println("rC um PKW auszuleihen");
		System.out.println("rM um Motorrad auszuleihen");
		System.out.println("rT um Transporter auszuleihen");
		System.out.println("returnC um PKW zurückzugeben");
		System.out.println("returnM um Motorrad zurückzugeben");
		System.out.println("returnT um Transporter zurückzugeben");
		while(true)
		{
			
			String rs = sc.nextLine();
			if(rs.equals("rC"))
			{
				vlv.rent_a_PKW();
			}
			else if(rs.equals("rM"))
			{
				vlv.rent_a_Motorrad();
			}
			else if(rs.equals("rT"))
			{
				vlv.rent_a_Transporter();
			}
			else if(rs.equals("returnM"))
			{
				String lp = sc.nextLine();
				vlv.return_a_Motorrad(lp);
			}
			else if(rs.equals("returnC"))
			{
				String lp = sc.nextLine();
				vlv.return_a_PKW(lp);
			}
			else if(rs.equals("returnT"))
			{
				String lp = sc.nextLine();
				vlv.return_a_Transporter(lp);
			}
		}
		
	
	
	
	}

}