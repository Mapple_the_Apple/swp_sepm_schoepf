public class Transporter extends Fahrzeug {

	private int sitze;
	private int ladefläche;

	public Transporter(String hersteller2, int baujahr2,String modell2, String nummer2, int sitze2, int ladefläche2) {
		super(hersteller2, "Transporter", baujahr2, modell2, nummer2);
		this.sitze = sitze2;
		ladefläche = ladefläche2;
	}

	public Transporter() {
		super();
	}

	public int getSeats() {
		return sitze;
	}

	public int getLoadarea() {
		return ladefläche;
	}
}