public class PKW extends Fahrzeug{
	
	private String extras;
	public PKW(String hersteller2, int baujahr2,String modell2, String nummer2, String extras2) {
		super(hersteller2, "PKW", baujahr2,modell2, nummer2);
		extras = extras2;
	}

	public PKW() {
		super();
	}

	public String getExtras()
	{
		return this.extras;
	}

}