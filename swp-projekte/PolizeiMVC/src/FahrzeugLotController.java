import java.util.ArrayList;

public class FahrzeugLotController {

	ArrayList <PKW> PKWs = new ArrayList<PKW>();
	ArrayList <Motorrad> motorrad = new ArrayList<Motorrad>();
	ArrayList <Transporter> transporter = new ArrayList<Transporter>();
	
	public String rentPKW()
	{
		for(int i= 0; i<PKWs.size(); i++)
		{
			
			if(!PKWs.get(i).getRented())
			{
				PKWs.get(i).setRented(true);
				return PKWs.get(i).getKennzeichen();
			}
		}
		return null;
	}
	public String rentMotorrad()
	{
		for(int i= 0; i<motorrad.size(); i++)
		{
			
			if(!motorrad.get(i).getRented())
			{
				motorrad.get(i).setRented(true);
				return motorrad.get(i).getKennzeichen();
			}
		}
		return null;
	}
	public String rentTransporter()
	{
		for(int i= 0; i<transporter.size(); i++)
		{
			
			if(!transporter.get(i).getRented())
			{
				transporter.get(i).setRented(true);
				return transporter.get(i).getKennzeichen();
			}
		}
		return null;
	}
	
	public void addPKW(PKW v)
	{
		PKWs.add(v);
	}
	public void addTransporter(Transporter t)
	{
		transporter.add(t);
	}
	public void addMotorrad( Motorrad m)
	{
		motorrad.add(m);
	}
	public boolean returnTransporter(String Kennzeichen)
	{
		for(int i= 0; i<transporter.size(); i++)
		{
			
			if((Kennzeichen.equals(transporter.get(i).getKennzeichen()))&&(transporter.get(i).getRented()))
			{
				transporter.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnMotorrad(String Kennzeichen)
	{
		for(int i= 0; i<motorrad.size(); i++)
		{
			
			if((Kennzeichen.equals(motorrad.get(i).getKennzeichen()))&&(motorrad.get(i).getRented()))
			{
				motorrad.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnPKW(String Kennzeichen)
	{
		for(int i= 0; i<PKWs.size(); i++)
		{
			
			if((Kennzeichen.equals(PKWs.get(i).getKennzeichen()))&&(PKWs.get(i).getRented()))
			{
				PKWs.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
}