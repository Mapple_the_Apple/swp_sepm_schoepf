
public class Fuhrpark {

	private int auto = 4;
	private int motorrad = 7;
	private int transporter = 2;
	
	public String ausleihenAutos(){
		auto--;
		if(auto>0)return ("Auto ausgeliehen.");
		else return ("Kein Auto mehr da!");
	}
	public String ausleihenMotorrad(){
		motorrad--;
		if(motorrad>0)return ("Motorrad ausgeliehen.");
		else return ("Kein Motorrad mehr da!");
	}
	public String ausleihenTransporter(){
		transporter--;
		if(transporter>0)return ("Transporter ausgeliehen.");
		else return ("Kein Transporter mehr da!");
	}
	
	public String zurückgebenAutos(){
		auto++;
		if(auto<=4)return ("Auto zurückgegeben.");
		else return ("Alle Autos da!");
	}
	public String zurückgebenMotorrad(){
		motorrad++;
		if(motorrad<=7)return ("Motorrad zurückgegeben.");
		else return ("Alle Motorräder da!");
	}
	public String zurückgebenTransporter(){
		transporter++;
		if(transporter<=2)return ("Transporter zurückgegeben.");
		else return ("Alle Transporter schon da!");
	}
	

}
