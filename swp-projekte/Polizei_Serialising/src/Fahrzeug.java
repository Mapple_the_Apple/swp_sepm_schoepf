public class Fahrzeug {
	private String typ;
	private String modell;
	private String kennzeichen;
	private String hersteller;
	private int baujahr;
	private boolean vermietet;

	public Fahrzeug(String hersteller, String typ , int baujahr,String modell, String kennzeichen)
	{
		this.typ = typ;
		this.baujahr=baujahr;
		this.hersteller=hersteller;
		this.baujahr=baujahr;
		this.kennzeichen=kennzeichen;
		this.modell=modell;
	}

	public Fahrzeug() {
		
	}
	public boolean getRented()
	{
		return this.vermietet;
	}
	public void setRented(boolean vermietet)
	{
		this.vermietet = vermietet;
	}
	public String getKennzeichen()
	{
		return kennzeichen;
	}
}