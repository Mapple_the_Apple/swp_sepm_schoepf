public class Motorrad extends Fahrzeug {

	private int ccm;
	public Motorrad(String hersteller2, int baujahr2,String modell2, String nummer2, int ccm2) {
		super(hersteller2, "Motorrad", baujahr2, modell2, nummer2);
		ccm = ccm2;
	}
	public Motorrad() {
		super();
	}
	public int getCcm() {
		return ccm;
	}
}