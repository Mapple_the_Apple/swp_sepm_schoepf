import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class FahrzeugLotController implements Serializable{

	ArrayList <PKW> PKWs = new ArrayList<PKW>();
	ArrayList <Motorrad> motorrad = new ArrayList<Motorrad>();
	ArrayList <Transporter> transporter = new ArrayList<Transporter>();
	
	public FahrzeugLotController(){}
	
	public void save(){
		XMLEncoder enc;
		try{
			enc=new XMLEncoder(new BufferedOutputStream(new FileOutputStream("/Users/florianbartenbach/Desktop/Serialising/Fahrzeuge.xml")));
			enc.writeObject(PKWs);
			enc.writeObject(motorrad);
			enc.writeObject(transporter);
			enc.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public void load(){
		XMLDecoder dec;
		try{
			dec = new XMLDecoder(new BufferedInputStream(new FileInputStream("/Users/florianbartenbach/Desktop/Serialising/Fahrzeuge.xml")));
			setPKW((ArrayList<PKW>)dec.readObject());
			setMotorrad((ArrayList<Motorrad>)dec.readObject());
			setTransporter((ArrayList<Transporter>)dec.readObject());
			dec.close();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	private void setTransporter(ArrayList<Transporter> readObject) {
		this.transporter= readObject;		
	}

	private void setMotorrad(ArrayList<Motorrad> readObject) {
		this.motorrad= readObject;		
	}

	private void setPKW(ArrayList<PKW> readObject) {
		this.PKWs= readObject;
		
	}

	public String rentPKW()
	{
		for(int i= 0; i<PKWs.size(); i++)
		{
			
			if(!PKWs.get(i).getRented())
			{
				PKWs.get(i).setRented(true);
				return PKWs.get(i).getKennzeichen();
			}
		}
		return null;
	}
	public String rentMotorrad()
	{
		for(int i= 0; i<motorrad.size(); i++)
		{
			
			if(!motorrad.get(i).getRented())
			{
				motorrad.get(i).setRented(true);
				return motorrad.get(i).getKennzeichen();
			}
		}
		return null;
	}
	public String rentTransporter()
	{
		for(int i= 0; i<transporter.size(); i++)
		{
			
			if(!transporter.get(i).getRented())
			{
				transporter.get(i).setRented(true);
				return transporter.get(i).getKennzeichen();
			}
		}
		return null;
	}
	
	public void addPKW(PKW v)
	{
		PKWs.add(v);
	}
	public void addTransporter(Transporter t)
	{
		transporter.add(t);
	}
	public void addMotorrad( Motorrad m)
	{
		motorrad.add(m);
	}
	public boolean returnTransporter(String Kennzeichen)
	{
		for(int i= 0; i<transporter.size(); i++)
		{
			
			if((Kennzeichen.equals(transporter.get(i).getKennzeichen()))&&(transporter.get(i).getRented()))
			{
				transporter.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnMotorrad(String Kennzeichen)
	{
		for(int i= 0; i<motorrad.size(); i++)
		{
			
			if((Kennzeichen.equals(motorrad.get(i).getKennzeichen()))&&(motorrad.get(i).getRented()))
			{
				motorrad.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
	public boolean returnPKW(String Kennzeichen)
	{
		for(int i= 0; i<PKWs.size(); i++)
		{
			
			if((Kennzeichen.equals(PKWs.get(i).getKennzeichen()))&&(PKWs.get(i).getRented()))
			{
				PKWs.get(i).setRented(false);
				return true;
			}
		}
		return false;
	}
}