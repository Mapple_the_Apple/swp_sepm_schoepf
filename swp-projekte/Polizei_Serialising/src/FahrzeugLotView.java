public class FahrzeugLotView{
	PKW c = new PKW();
	Motorrad m = new Motorrad();
	Transporter t = new Transporter();
	FahrzeugLotController vlc = new FahrzeugLotController();
	String Kennzeichen;
	
	
	
	public void add_new_Vehicle(Object v)
	{
		
		if(v.getClass().equals(c.getClass()))
		{
			vlc.addPKW((PKW) v);
		}
		else if(v.getClass().equals(m.getClass()))
		{
			vlc.addMotorrad((Motorrad) v);
		}
		else if(v.getClass().equals(t.getClass()))
		{
			vlc.addTransporter((Transporter) v);
		}
	}
	public void rent_a_PKW()
	{
		Kennzeichen = null;
		Kennzeichen = vlc.rentPKW();
		if(Kennzeichen != null)
		{
			System.out.println("Das Fahrzeug "+Kennzeichen+" wurde ausgeliehen.");
		}
		else
		{
			System.out.println("Es tut uns sehr leid aber es stehen zur Zeit keine PKWs zur Verfügung.");
		}
	}
	public void rent_a_Motorrad()
	{
		Kennzeichen = null;
		Kennzeichen = vlc.rentMotorrad();
		if(Kennzeichen != null)
		{
			System.out.println("Das Fahrzeug mit dem Kennzeichen "+Kennzeichen+" wurde ausgeliehen.");
		}
		else
		{
			System.out.println("Es tut uns sehr leid aber es stehen zur Zeit keine Motorrads zur Verfügung.");
		}
	}
	public void rent_a_Transporter()
	{
		Kennzeichen = null;
		Kennzeichen = vlc.rentTransporter();
		if(Kennzeichen != null)
		{
			System.out.println("The Transporter mit dem Kennzeichen "+Kennzeichen+" wurde ausgeliehen.");
		}
		else
		{
			System.out.println("Es tut uns sehr leid aber es stehen zur Zeit keine Transporter zur Verfügung.");
		}
	}
	public void return_a_PKW(String Kennzeichen)
	{
		if(vlc.returnPKW(Kennzeichen))
		{
			System.out.println("Das Fahrzeug mit dem Kennzeichen "+Kennzeichen+" wurde erfolgreich zurückgegeben.");
		}
		else
		{
			System.out.println("Das Fahrzeug ist nicht registriert.");
		}
		
	}
	public void return_a_Motorrad(String Kennzeichen)
	{
		if(vlc.returnMotorrad(Kennzeichen))
		{
			System.out.println("Das Fahrzeug mit dem Kennzeichen "+Kennzeichen+" wurde erfolgreich zurückgegeben.");
		}
		else
		{
			System.out.println("Das Fahrzeug ist nicht registriert.");
		}
	}
	public void return_a_Transporter(String Kennzeichen)
	{
		if(vlc.returnTransporter(Kennzeichen))
		{
			System.out.println("Das Fahrzeug mit dem Kennzeichen "+Kennzeichen+" wurde erfolgreich zurückgegeben.");
		}
		else
		{
			System.out.println("Das Fahrzeug ist nicht registriert.");
		}
	}

}